*** Settings ***
Library         SeleniumLibrary

*** Test Cases ***
## steps
Dado que acesso a página login InsureMO
    Open Browser                about:blank     headlesschrome      options=add_argument('--disable-dev-shm-usage')
    Go To                       https://stcici-gitst.insuremo.com/ui/admin/#/login
    Title Should Be             Admin

Devo Logar no Ambiente de SIT com sucesso
    Click Element                       xpath://*[@id="app"]/div/div[1]/div/div/input[1]
    Input Text                          xpath://*[@id="app"]/div/div[1]/div/div/input[1]        ADMIN
    Input Text                          xpath://*[@id="app"]/div/div[1]/div/div/input[2]        eBao1234
    Click Element                       xpath://*[@id="login_content_button"]/div 
    Capture Page Screenshot

Abir o Policy Admin do insureMO para Iniciar uma Quote
    Wait Until Page Contains            Policy Administration
    Click Element                       xpath://*[@id="30003"]/a/div/span[1]/span[2]
    Click Element                       xpath://*[@id="30003"]/a/div/span[1]/span[2]
    Wait Until Page Contains            Policy Management
    Click Element                       xpath://*[@id="370544015"]/a/span

Fechar o Browser da Quote
    Capture Page Screenshot
    Close Browser    